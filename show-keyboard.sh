#!/bin/bash

switch_toggle () {
  STORE=$(cat "/tmp/switch-keyboard.temp")
  if [ $STORE == 'dvorak' ]
  then
    LANG=$(echo 'en')
    echo "$LANG" > /tmp/switch-keyboard.temp
    hyprctl keyword input:kb_layout "us" > /dev/null
  elif [ $STORE == 'en' ]
  then
    LANG=$(echo 'cz')
    echo "$LANG" > /tmp/switch-keyboard.temp
    hyprctl keyword input:kb_layout "cz(qwerty)" > /dev/null 
  else

    LANG=$(echo 'dvorak')
    echo "$LANG" > /tmp/switch-keyboard.temp
    hyprctl keyword input:kb_layout "us(dvorak)" > /dev/null 
  fi

  notify-send --expire-time=700 "Keyboard switched to $LANG"

}

switch_set () {
  echo "$1" > /tmp/switch-keyboard.temp
  if [ $1 == 'dvorak' ]
  then
    hyprctl keyword input:kb_layout "us(dvorak)" > /dev/null 
  elif [ $1 == 'cz' ]
  then
    hyprctl keyword input:kb_layout "cz(qwerty)" > /dev/null 
  else
    hyprctl keyword input:kb_layout "us" > /dev/null 
  fi

  notify-send --expire-time=700 "Keyboard switched to $1"
}

if [ "$1" = 'toggle' ]
then
  switch_toggle
  exit 0
else
  switch_set $1
fi

