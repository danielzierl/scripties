#!/bin/bash
if ps -e | grep -w "waybar$" > /dev/null; then
  killall waybar;
else
  waybar & 
fi
